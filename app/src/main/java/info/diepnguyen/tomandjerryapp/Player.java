package info.diepnguyen.tomandjerryapp;

/**
 * Created by thienho on 12/23/17.
 */

public class Player {
    double latitude;
    double longitude;

    public Player (){}

    public Player(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
