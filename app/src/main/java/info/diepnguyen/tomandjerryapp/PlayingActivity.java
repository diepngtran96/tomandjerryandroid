package info.diepnguyen.tomandjerryapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import info.diepnguyen.tomandjerryapp.Calculate.DistanceCalculator;
import info.diepnguyen.tomandjerryapp.Gameplay.GamePlay;

public class PlayingActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final int MY_PERMISSION_REQUEST_CODE = 7171;
    private static final int PLAY_SERVICES_RES_REQUEST = 7172;
    private static int UPDATE_INTERVAL = 5000;
    private static int FASTEST_INTERVAL = 3000;
    private static int DISTANCE = 10;

    private final String TAG = "PlayingActivity";
    Marker mJerryMarker;
    Marker mTomMarker;
    Switch readySwitch;
    TextView statusTextView, distanceTextView, timerTextView;
    private GoogleMap mMap;
    private DatabaseReference playersDB;
    private DatabaseReference timeDB;
    private List<Player> playersArray;
    private Player mJerryPlayer, mTomPlayer;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private int timeSystem, timeFirebase;
    private Handler handler;
    private Runnable runnable;

    GamePlay gamePlay;
    int gameResult = 0;
    private int countDown = 60;
    int takeTime = 0;

    double distance;

    private AnimationDrawable playerAnimation;
    private ImageView playerImage;

    int height = 50;
    int width = 50;
    int playResult = 0;

    //Sound
    MediaPlayer gameoverMP ;
    MediaPlayer goMP ;
    MediaPlayer readyMP ;
    MediaPlayer timeroverMP ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Sound effect
        gameoverMP = MediaPlayer.create(this, R.raw.gameover);
        goMP = MediaPlayer.create(this, R.raw.go);
        readyMP = MediaPlayer.create(this, R.raw.ready);
        timeroverMP = MediaPlayer.create(this, R.raw.timeover);

        Log.d(TAG, "onCreate.");

        // Reference database
        playersDB = FirebaseDatabase.getInstance().getReference().child("Thien").child("Players");
        // Reference Button Click

        //Init view
        readySwitch = findViewById(R.id.sw_ready);
        readySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.d(TAG, "sw_ready: isChecked");
                    // Change status
                    if (mJerryPlayer.latitude == 0) {
                        statusTextView.setText("Jerry");

                        //Animation ref
                        playerImage = findViewById(R.id.animation_image);
                        playerImage.setBackgroundResource(R.drawable.animation_jerry);
                        playerAnimation = (AnimationDrawable) playerImage.getBackground();



                    } else if (mTomPlayer.latitude == 0) {
                        statusTextView.setText("Tom");

                        // Animation ref
                        playerImage = findViewById(R.id.animation_image);
                        playerImage.setBackgroundResource(R.drawable.animation_tom);
                        playerAnimation = (AnimationDrawable) playerImage.getBackground();



                    } else {
                        statusTextView.setText("Error");
                    }

                    // setup location and show marker
                    setUpLocation();

                    //Run animation
                    if (!playerAnimation.isRunning()) {
                        playerAnimation.run();
                    }

                } else {
                    // Stop update location
                    stopLocationUpdates();
                    // Reset Firebase data
                    Player resetPlayer = new Player(0, 0);
                    if (statusTextView.getText().equals("Jerry")) {
                        //Jerry's Marker
                        if (mJerryMarker != null) {
                            mJerryMarker.remove();
                        }
                        if (mTomMarker != null) {
                            mTomMarker.remove();
                        }
                        playersDB.child("Jerry").setValue(resetPlayer);
                        statusTextView.setText("Not ready");

                    } else if (statusTextView.getText().equals("Tom")) {
                        //Marker for Tom
                        if (mTomMarker != null) {
                            mTomMarker.remove();
                        }
                        if (mJerryMarker != null) {
                            mJerryMarker.remove();
                        }

                        playersDB.child("Tom").setValue(resetPlayer);
                        statusTextView.setText("Not ready");
                    } else {
                        statusTextView.setText("Not ready");
                    }

                    // Clear Distance
                    distanceTextView.setText("");
                    if (gameResult == 1) {
                        showJerryWinAlert();
                        gameResult = 0;
                    }
                    else if(gameResult == 2) {
                        showTomWinAlert();
                        gameResult = 0;
                    }

                    Log.d(TAG, "sw_ready: unChecked.");

                    // Stop Animation
                    if(playerAnimation.isRunning()){
                        playerAnimation.stop();
                    }

                    // Reset take time
                    takeTime = 0;

                    // Stop timer
                    if(runnable != null){
                        handler.removeCallbacks(runnable);
                        timerTextView.setText("");
                    }

                }
            }
        });

        // Reference TextView
        statusTextView = findViewById(R.id.tv_status);
        distanceTextView = findViewById(R.id.tv_distance);
        timerTextView = findViewById(R.id.tv_timer);

        // Reference Players Array
        playersArray = new ArrayList<>();

        // Reference Gameplay
        gamePlay = new GamePlay();



    }


    // TODO: Get FirebaseTime
    private void getFirebaseTime(){
        timeDB = FirebaseDatabase.getInstance().getReference().child("Thien").child("startTime");

        timeDB.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Firebase time
                timeFirebase = Integer.parseInt(dataSnapshot.getValue().toString()) + 10;
                Log.e(TAG, "Firebase Time: " + dataSnapshot.getValue());

                // Get system time
                timeSystem = (int) (System.currentTimeMillis() / 1000);
                Log.e(TAG, "System Time: " + timeSystem);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if(timeSystem == (timeFirebase - 1)){
                    timerTextView.setText("GO");
                    goMP.start();
                }
                if (timeSystem < timeFirebase){
                    timeSystem = (int) (System.currentTimeMillis() / 1000);
                    Log.e(TAG, "System Time: " + timeSystem);
                    handler.postDelayed(this, 1000);
                } else {
                    timer();
                }
            }
        };
        handler.postDelayed(runnable, 1000);
        timerTextView.setText("READY");
        readyMP.start();


    }

    private void resetPlayer(){
        // Reset Firebase data
        Player resetPlayer = new Player(0, 0);
        playersDB.child("Jerry").setValue(resetPlayer);
        playersDB.child("Tom").setValue(resetPlayer);


    }

    // TODO: Timer Declare
    private void timer(){
        Log.d(TAG, "Start Timer");
        runnable = new Runnable() {
            @Override
            public void run() {
                if(countDown > 0){
                    countDown--;
                    handler.postDelayed(this, 1000);    // Every Seconds
                    Log.e(TAG, "Play Timer: " + countDown);
                    timerTextView.setText(countDown + " s");

                    //Start gameplay
                    gameResult = gamePlay.checkPlayer2(mJerryPlayer,mTomPlayer,distance,10.0,countDown);
                    Log.d("Game Play","Game result " + gameResult);
                    //TODO: When Jerry switch off Tom win
                    if(mJerryPlayer.latitude == 0 ){
                        gameResult = 2;
                    }
                    switch (gameResult) {
                        case 1:
                            Log.d(TAG, "EndGame: " + "Jerry Win");
                            timerTextView.setText("");
                            readySwitch.setChecked(false);
                            handler.removeCallbacks(this);
                            timeroverMP.start();
                            // TODO: Jerry Win
                            break;
                        case 2:
                            //FIXME: still display marker while end game
                            Log.d(TAG, "EndGame: " + "Tom Win");
                            timerTextView.setText("");
                            readySwitch.setChecked(false);
                            Log.d("End Game: ","Jerry Location "+ mJerryPlayer.latitude);
                            handler.removeCallbacks(this);
                            countDown = 60;
                            break;
                        case 0:
                            Log.d(TAG, "Playing ......");
                            break;

                    }

                } else {
                    Log.d(TAG, "Timer: Timer Completed");
                    countDown = 60;
                    timerTextView.setText("");
                    Log.e(TAG, "Player WIN: Jerry");
                }
            }
        };
        countDown = 60;
        handler.postDelayed(runnable, 1000);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayServices()) {
                        buildGoogleApiClient();
                        createLocationRequest();
                        if (readySwitch.isChecked()) {
                            displayLocation();
                        }

                    }

                }
        }
    }

    private void setUpLocation() {
        // Request permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            if (checkPlayServices()) {
                buildGoogleApiClient();
                createLocationRequest();

                if (readySwitch.isChecked()) {
                    displayLocation();

                }

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart.");

        // Connect GoogleApiClient
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            Log.i(TAG, "GoogleApiClient: connect.");
        }

        // Retrieve and update PlayerDB
        playersDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Clear player list
                playersArray.clear();

                // Get data from firebase
                for (DataSnapshot playerSnapshot : dataSnapshot.getChildren()) {
                    Player player = playerSnapshot.getValue(Player.class);
                    playersArray.add(player);

                    Log.d(TAG, "Firebase: Player: " + player.latitude + " - " + player.longitude);
                }
                // Get Jerry and Tom location
                mJerryPlayer = new Player(playersArray.get(0).latitude, playersArray.get(0).longitude);
                mTomPlayer = new Player(playersArray.get(1).latitude, playersArray.get(1).longitude);

                Log.d(TAG, "Firebase: Jerry: " + mJerryPlayer.latitude + " - " + mJerryPlayer.longitude);
                Log.d(TAG, "Firebase: Tom: " + mTomPlayer.latitude + " - " + mTomPlayer.longitude);


                // Update Display location
                Log.d("GPS:", "mGoogleApi: " + mGoogleApiClient + "- mLastLocation: " + mLastLocation);

                if (mGoogleApiClient != null) {
                    displayLocation();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    private boolean checkPlayServices() {
        int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(result)) {
                GooglePlayServicesUtil.getErrorDialog(result, this, PLAY_SERVICES_RES_REQUEST).show();
            } else {
                Toast.makeText(this, "This device is not supported", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setSmallestDisplacement(DISTANCE);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private void displayLocation() {
        // Check Permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            if (readySwitch.isChecked()) {
                final double latitude = mLastLocation.getLatitude();
                final double longitude = mLastLocation.getLongitude();

                // Update to Firebase
                Player currentPlayer = new Player(latitude, longitude);
                String currentPlayerName = statusTextView.getText().toString();
                playersDB.child(currentPlayerName).setValue(currentPlayer).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "DisplayLocation: " + mTomPlayer.latitude);
                        Log.d(TAG, "DisplayLocation:" + mJerryPlayer.latitude);


                        //Check 2 player marker
                        if (mJerryMarker != null) {
                            mJerryMarker.remove();
                        }
                        if (mTomMarker != null) {
                            mTomMarker.remove();
                        }


                        //Format size of Jerry Marker Icon
                        BitmapDrawable bitmapdrawJery = (BitmapDrawable)getResources().getDrawable(R.drawable.jerry);
                        Bitmap bJerry = bitmapdrawJery.getBitmap();
                        Bitmap jerryMarker = Bitmap.createScaledBitmap(bJerry, width, height, false);

                        //Set marker Jerry
                        mJerryMarker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mJerryPlayer.latitude, mJerryPlayer.longitude))
                                .title("Jerry")
                                .icon(BitmapDescriptorFactory.fromBitmap(jerryMarker)));


                        // Show info window
                        mJerryMarker.showInfoWindow();

                        //Rotate marker
//                        rotateMarker(mJerryMarker,90,mMap);

                        if (mTomPlayer.latitude != 0) {
                            // Calculate Distance
                            DistanceCalculator distanceCalculator = new DistanceCalculator();
                            distance = distanceCalculator.distance(
                                    mJerryPlayer.latitude, mJerryPlayer.longitude,
                                    mTomPlayer.latitude, mTomPlayer.longitude,
                                    "K") * 1000; // Convert to meter.
                            Log.d(TAG, "Distance: " + String.format("%.2f", distance)  + "m");

                            // Show Distance TextView
                            distanceTextView.setText(String.format("%.2f",distance) + " m");

                            //Format size of Jerry Marker Icon
                            BitmapDrawable bitmapdrawTom = (BitmapDrawable)getResources().getDrawable(R.drawable.tom);
                            Bitmap bTom = bitmapdrawTom.getBitmap();
                            Bitmap tomMarker = Bitmap.createScaledBitmap(bTom, width, height, false);


                            //Set marker Tom
                            mTomMarker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mTomPlayer.latitude, mTomPlayer.longitude))
                                    .title("Tom")
                                    .snippet("Distance: " + String.format("%.2f", distance) + "m")
                                    .icon(BitmapDescriptorFactory.fromBitmap(tomMarker)));
                            // Show info window
                            mTomMarker.showInfoWindow();
                            mJerryMarker.showInfoWindow();

                            // TODO: Get Current Time and Firebase
                            if (takeTime == 0) {
                                getFirebaseTime();
                                takeTime = 1;
                            }


                        } else {
                            distanceTextView.setText("");
                        }


                    }
                });

                //Move Camera to this position
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude)
                        , 17.0f));



            }
        } else {
            //Toast.makeText(this,"Couldn't get location",Toast.LENGTH_SHORT);
            Log.e(TAG, "Couldn't load the location");
            distanceTextView.setText("");
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdates();


    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Unchecked ready switch
        readySwitch.setChecked(false);

        //Disconnect Google API
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        Log.d("On Stop", "is running");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("On Destroy", "is running");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        Log.d("on Resume","is running");

    }

    private void showTomWinAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("End Game");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage("Tom is win !!!!");
        alertDialog.setNegativeButton("Out game", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                readySwitch.setChecked(false);
            }
        });
        alertDialog.show();

        // TODO: Clear marker with GoogleMap
        mMap.clear();
        Log.d(TAG, "showTomWinAlert: ClearMap");
    }

    private void showJerryWinAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("End Game");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage("Jerry is win !!!!");
        alertDialog.setNegativeButton("Out game", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                readySwitch.setChecked(false);
            }
        });
        alertDialog.show();
    }
}
