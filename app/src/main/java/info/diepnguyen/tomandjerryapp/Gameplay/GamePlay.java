package info.diepnguyen.tomandjerryapp.Gameplay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;

import info.diepnguyen.tomandjerryapp.Player;
import info.diepnguyen.tomandjerryapp.PlayingActivity;
import info.diepnguyen.tomandjerryapp.R;

/**
 * Created by thien on 1/3/2018.
 */

public class GamePlay {
    private static String TAG = "Gamplay";


    // 0 - Nothing, 1 - Jerry Win, 2 - Tom Win
    private int resultGameplay = 0;

    public GamePlay(){}

    public void checkPlayer(Player jerryPlayer, Player tomPlayer, double distance, double winDistance, int timeout){
        Log.d(TAG, "checkPlayer: checking");
        if(jerryPlayer.getLatitude() != 0 && tomPlayer.getLatitude() != 0){
            // Check distance below 5m
            if(distance <= winDistance && timeout > 0 ){
                resultGameplay = 2;     // Tom win
            } else if (distance > winDistance && timeout == 0){
                resultGameplay = 1;     // Jerry win
            } else {
                resultGameplay = 0;     // Keep playing
            }
        }
        endGameMessage(resultGameplay);
    }
    public int checkPlayer2(Player jerryPlayer, Player tomPlayer, double distance, double winDistance, int timeout){
        Log.d(TAG, "checkPlayer: checking");
        if(jerryPlayer.getLatitude() != 0 && tomPlayer.getLatitude() != 0){
            // Check distance below 5m
            if(distance <= winDistance && timeout > 0 || jerryPlayer.getLatitude() == 0){
                resultGameplay = 2;     // Tom win
            } else if (distance > winDistance && timeout == 0 || tomPlayer.getLatitude() == 0){
                resultGameplay = 1;     // Jerry win
            } else {
                resultGameplay = 0;     // Keep playing
            }
        }
        else if (tomPlayer.getLatitude() == 0){
            resultGameplay = 1;
        }
        return resultGameplay;
    }

    public void endGameMessage(int resultGameplay){
        Log.d(TAG, "EndGame...");
        switch (resultGameplay){
            case 1:
                Log.d(TAG, "EndGame: " + "Jerry Win");
                break;
            case 2:
                Log.d(TAG, "EndGame: " + "Tom Win");
                break;
            case 0:
                Log.d(TAG, "Playing ......");
                break;
        }
    }


}
